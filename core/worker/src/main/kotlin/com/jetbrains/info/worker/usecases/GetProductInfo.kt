package com.jetbrains.info.worker.usecases

import com.jetbrains.info.utils.ProgressInputStream
import com.jetbrains.info.worker.usecases.GetProductInfo.ProgressListener
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
class GetProductInfo(
    private val restTemplate: RestTemplate,
    private val getFileFromArchive: GetFileFromArchive,
) {
    operator fun invoke(
        link: String,
        progressListener: ProgressListener = ProgressListener { _, _ -> }
    ): Result<String> = runCatching {
        restTemplate.execute(
            /* url = */
            link,
            /* method = */
            HttpMethod.GET,
            /* requestCallback = */
            null,
            /* responseExtractor = */
            { response ->
                val size = response.headers[HttpHeaders.CONTENT_LENGTH]
                    ?.firstOrNull()
                    ?.toLongOrNull()
                val inputStream = ProgressInputStream(
                    inputStream = response.body,
                    notifyEachNBytes = 1024 * 1024,
                    listener = { progressListener.onProgress(readBytes = it, totalBytes = size) },
                )
                getFileFromArchive(stream = inputStream, fileName = FILENAME).getOrThrow()
            }
        )
    }

    fun interface ProgressListener {
        fun onProgress(readBytes: Long, totalBytes: Long?)
    }

    companion object {
        const val FILENAME = "product-info.json"
    }
}