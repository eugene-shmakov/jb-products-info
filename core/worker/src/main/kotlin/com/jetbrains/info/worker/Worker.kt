package com.jetbrains.info.worker

import com.jetbrains.info.domain.listeners.DownloadQueueListener
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.model.ReleaseInfo.Status
import com.jetbrains.info.domain.model.WorkerStatus.Downloading
import com.jetbrains.info.domain.usecases.ExecuteWork
import com.jetbrains.info.domain.usecases.storage.GetAndUpdateEnqueuedReleaseInfo
import com.jetbrains.info.domain.usecases.storage.StoreReleaseInfo
import com.jetbrains.info.extensions.getLogger
import com.jetbrains.info.worker.usecases.GetProductInfo
import org.springframework.stereotype.Component

@Component
class Worker(
    private val executeWork: ExecuteWork,
    private val getAndUpdateEnqueuedReleaseInfo: GetAndUpdateEnqueuedReleaseInfo,
    private val storeReleaseInfo: StoreReleaseInfo,
    private val getProductInfo: GetProductInfo,
) : DownloadQueueListener {
    private val log by getLogger()

    override fun onNewReleaseAvailable(release: ReleaseInfo.Key) {
        download(release)
    }

    private fun download(release: ReleaseInfo.Key) {
        executeWork {
            val releaseInfo = getAndUpdateEnqueuedReleaseInfo(release) ?: return@executeWork

            val link = releaseInfo.release.link
            log.debug("Started '$link' downloading")
            reportStatus(Downloading(releaseInfo))

            val result = getProductInfo(
                link = link,
                progressListener = { readBytes, totalBytes ->
                    reportStatus(Downloading(releaseInfo, readBytes = readBytes, totalBytes = totalBytes))
                }
            )

            storeReleaseInfo(
                if (result.isFailure) {
                    val exception = result.exceptionOrNull()
                    log.error("Failed to download '$link'", exception)
                    releaseInfo.copy(
                        status = Status.ERROR,
                        errorReason = exception?.message,
                    )
                } else {
                    log.debug("Completed '$link' downloading")
                    releaseInfo.copy(
                        status = Status.READY,
                        jsonInfo = result.getOrNull(),
                    )
                }
            )
        }
    }
}