package com.jetbrains.info.worker.repository

import com.jetbrains.info.domain.model.WorkerStatus
import com.jetbrains.info.domain.repository.WorkerStatusRepository
import org.springframework.stereotype.Component
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

@Component
internal class WorkerStatusRepositoryImpl : WorkerStatusRepository {

    private val lock = ReentrantReadWriteLock()
    private val workers = mutableMapOf<String, WorkerStatus>()

    override fun reportStatus(workerId: String, status: WorkerStatus) {
        lock.write { workers[workerId] = status }
    }

    override fun removeStatus(workerId: String) {
        lock.write { workers -= workerId }
    }

    override fun getOverallStatus(): List<WorkerStatus> =
        lock.read { workers.values.toList() }
}