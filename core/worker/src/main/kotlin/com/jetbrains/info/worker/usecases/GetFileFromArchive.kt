package com.jetbrains.info.worker.usecases

import com.jetbrains.info.extensions.getLogger
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream
import org.springframework.stereotype.Component
import java.io.FileNotFoundException
import java.io.InputStream

@Component
class GetFileFromArchive {
    private val log by getLogger()

    operator fun invoke(stream: InputStream, fileName: String): Result<String> = runCatching {
        val tarInput = TarArchiveInputStream(GzipCompressorInputStream(stream))
        sequence<TarArchiveEntry> {
            var currentEntry = tarInput.nextTarEntry
            while (currentEntry != null) {
                yield(currentEntry)
                currentEntry = tarInput.nextTarEntry
            }
        }
            .onEach { log.trace("file: '${it.name}'") }
            .firstOrNull { it.name.substringAfter("/") == fileName }
            ?.let { tarInput.reader().readText() }
            ?: throw FileNotFoundException(fileName)
    }
}
