package com.jetbrains.info.worker

import com.jetbrains.info.annotation.UnitTest
import com.jetbrains.info.domain.model.Release
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.model.ReleaseInfo.Status.*
import com.jetbrains.info.domain.model.create
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import com.jetbrains.info.domain.usecases.ExecuteWork
import com.jetbrains.info.domain.usecases.storage.GetAndUpdateEnqueuedReleaseInfo
import com.jetbrains.info.domain.usecases.storage.StoreReleaseInfo
import com.jetbrains.info.worker.usecases.GetProductInfo
import io.mockk.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

@UnitTest
internal class WorkerTest {
    private val getProductInfo: GetProductInfo = mockk()
    private val releaseInfoRepository = mockk<ReleaseInfoRepository> {
        every { save(any<ReleaseInfo>()) } just runs
    }
    private val worker = Worker(
        executeWork = ExecuteWork(mockk(relaxed = true)),
        getAndUpdateEnqueuedReleaseInfo = GetAndUpdateEnqueuedReleaseInfo(releaseInfoRepository),
        storeReleaseInfo = StoreReleaseInfo(releaseInfoRepository),
        getProductInfo = getProductInfo,
    )

    @Test
    fun downloadFailure() {
        every { releaseInfoRepository.findById(releaseInfo.id) } returns releaseInfo
        every { getProductInfo.invoke(link, any()) } returns Result.failure(exception)
        assertDoesNotThrow {
            worker.onNewReleaseAvailable(releaseInfo.id)
        }

        verifyOrder {
            releaseInfoRepository.findById(releaseInfo.id)
            releaseInfoRepository.save(releaseInfo.copy(status = DOWNLOADING))
            releaseInfoRepository.save(releaseInfo.copy(status = ERROR, errorReason = exception.message))
        }
    }

    @ParameterizedTest
    @EnumSource(ReleaseInfo.Status::class)
    fun download(status: ReleaseInfo.Status) {
        every { getProductInfo.invoke(link, any()) } returns Result.success(json)
        every { releaseInfoRepository.findById(releaseInfo.id) } returns
                releaseInfo.copy(
                    status = status,
                    errorReason = exception.message.takeIf { status == ERROR }
                )

        worker.onNewReleaseAvailable(releaseInfo.id)

        verifyOrder {
            releaseInfoRepository.findById(releaseInfo.id)
            if (status != READY) {
                releaseInfoRepository.save(releaseInfo.copy(status = DOWNLOADING))
                releaseInfoRepository.save(releaseInfo.copy(status = READY, jsonInfo = json))
            }
        }
    }

    companion object {
        private const val link = "some link to download"
        private const val json = "some json"
        private val releaseInfo = ReleaseInfo.create(release = Release.create(link = link), status = ENQUEUED)
        private val exception = RuntimeException("some error")
    }
}