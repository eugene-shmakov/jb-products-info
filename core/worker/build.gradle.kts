description = "Message queue"

dependencies {
    implementation(project(Projects.Core.UTILS))
    implementation(project(Projects.Domain.REPOSITORIES))
    implementation(project(Projects.Domain.USE_CASES))
    implementation(Deps.Kotlin.stdlib)
    implementation(Deps.Kotlin.stdlibJdk8)
    implementation(Deps.ApacheCommons.compress)
    implementation(Deps.Spring.Boot.web)
    implementation(Deps.Spring.Core.context)
    implementation(Deps.Spring.Data.commons)

    testImplementation(testFixtures(project(Projects.Core.UTILS)))
    testImplementation(testFixtures(project(Projects.Domain.MODEL)))
}