package com.jetbrains.info.db.mapper

import com.jetbrains.info.db.model.ProductRefreshInfoDao
import com.jetbrains.info.domain.model.ProductRefreshInfo
import com.jetbrains.info.utils.mapper.Mapper

internal object ProductRefreshInfoDaoMapper : Mapper<ProductRefreshInfoDao, ProductRefreshInfo> {
    override fun ProductRefreshInfoDao.mapEntity() =
        ProductRefreshInfo(
            code = code,
            lastRefreshTime = lastRefreshTime
        )
}