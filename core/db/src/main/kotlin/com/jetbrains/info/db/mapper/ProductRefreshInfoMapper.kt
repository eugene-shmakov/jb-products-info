package com.jetbrains.info.db.mapper

import com.jetbrains.info.db.model.ProductRefreshInfoDao
import com.jetbrains.info.domain.model.ProductRefreshInfo
import com.jetbrains.info.utils.mapper.Mapper

internal object ProductRefreshInfoMapper : Mapper<ProductRefreshInfo, ProductRefreshInfoDao> {
    override fun ProductRefreshInfo.mapEntity() =
        ProductRefreshInfoDao(
            code = code,
            lastRefreshTime = lastRefreshTime
        )
}