package com.jetbrains.info.db.repository.mongo

import com.jetbrains.info.db.model.ProductRefreshInfoDao
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
internal interface ProductRefreshMongoRepository : CrudRepository<ProductRefreshInfoDao, String> {
    override fun findAll(): List<ProductRefreshInfoDao>
}
