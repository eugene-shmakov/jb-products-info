package com.jetbrains.info.db.mapper

import com.jetbrains.info.db.model.ReleaseInfoDao
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.utils.mapper.Mapper

internal object ReleaseInfoDaoMapper : Mapper<ReleaseInfoDao, ReleaseInfo> {
    override fun ReleaseInfoDao.mapEntity() =
        ReleaseInfo(
            product = product,
            release = release,
            status = status,
            jsonInfo = jsonInfo,
            errorReason = errorReason,
            id = id
        )
}