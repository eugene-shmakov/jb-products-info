package com.jetbrains.info.db.model

import com.jetbrains.info.domain.model.Release
import com.jetbrains.info.domain.model.ReleaseInfo
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("releaseInfo")
internal data class ReleaseInfoDao(
    val product: ReleaseInfo.Product,
    val release: Release,
    val status: ReleaseInfo.Status,
    val jsonInfo: String?,
    val errorReason: String?,
    @Id
    val id: ReleaseInfo.Key = ReleaseInfo.Key.of(product, release)
)
