package com.jetbrains.info.db.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDateTime

@Document("productRefreshInfo")
internal data class ProductRefreshInfoDao(
    @Id
    val code: String,
    val lastRefreshTime: LocalDateTime
)
