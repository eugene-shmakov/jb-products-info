package com.jetbrains.info.db.repository

import com.jetbrains.info.db.mapper.ReleaseInfoDaoMapper
import com.jetbrains.info.db.mapper.ReleaseInfoMapper
import com.jetbrains.info.db.repository.mongo.ReleaseInfoMongoRepository
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import org.springframework.stereotype.Component
import java.util.stream.Stream
import kotlin.jvm.optionals.getOrNull

@OptIn(ExperimentalStdlibApi::class)
@Component
internal class ReleaseInfoRepositoryImpl(
    private val repository: ReleaseInfoMongoRepository
) : ReleaseInfoRepository {
    private val domainMapper = ReleaseInfoMapper
    private val daoMapper = ReleaseInfoDaoMapper

    override fun findById(id: ReleaseInfo.Key): ReleaseInfo? =
        repository.findById(id).getOrNull()?.let(daoMapper)

    override fun findAll(): Stream<ReleaseInfo> =
        repository.findAllBy().map(daoMapper)

    override fun findByCodeHavingJsonInfo(code: String): List<ReleaseInfo> =
        repository.findAllByProductCodeAndJsonInfoNotNull(code).map(daoMapper)

    override fun findByCodeAndBuildNumber(code: String, buildNumber: String): ReleaseInfo? =
        repository.findByProductCodeAndReleaseBuild(code, buildNumber)?.let(daoMapper)

    override fun save(releaseInfo: ReleaseInfo) {
        repository.save(releaseInfo.let(domainMapper))
    }

    override fun save(releases: List<ReleaseInfo>) {
        repository.saveAll(releases.map(domainMapper))
    }
}