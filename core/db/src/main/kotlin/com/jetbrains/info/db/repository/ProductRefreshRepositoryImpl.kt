package com.jetbrains.info.db.repository

import com.jetbrains.info.db.mapper.ProductRefreshInfoDaoMapper
import com.jetbrains.info.db.mapper.ProductRefreshInfoMapper
import com.jetbrains.info.db.repository.mongo.ProductRefreshMongoRepository
import com.jetbrains.info.domain.model.ProductRefreshInfo
import com.jetbrains.info.domain.repository.ProductRefreshRepository
import org.springframework.stereotype.Component

@Component
internal class ProductRefreshRepositoryImpl(
    private val repository: ProductRefreshMongoRepository
) : ProductRefreshRepository {
    private val domainMapper = ProductRefreshInfoMapper
    private val daoMapper = ProductRefreshInfoDaoMapper

    override fun findAll(): List<ProductRefreshInfo> =
        repository.findAll().map(daoMapper)

    override fun save(products: List<ProductRefreshInfo>) {
        repository.saveAll(products.map(domainMapper))
    }
}