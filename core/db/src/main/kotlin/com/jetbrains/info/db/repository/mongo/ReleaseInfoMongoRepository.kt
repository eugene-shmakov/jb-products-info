package com.jetbrains.info.db.repository.mongo

import com.jetbrains.info.db.model.ReleaseInfoDao
import com.jetbrains.info.domain.model.ReleaseInfo
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.stream.Stream

@Repository
internal interface ReleaseInfoMongoRepository : CrudRepository<ReleaseInfoDao, ReleaseInfo.Key> {
    fun findAllBy(): Stream<ReleaseInfoDao>

    fun findAllByProductCodeAndJsonInfoNotNull(code: String): List<ReleaseInfoDao>

    fun findByProductCodeAndReleaseBuild(code: String, buildNumber: String): ReleaseInfoDao?
}
