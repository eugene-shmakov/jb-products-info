description = "Storage layer"

dependencies {
    implementation(project(Projects.Core.UTILS))
    implementation(project(Projects.Domain.REPOSITORIES))
    implementation(Deps.Kotlin.stdlib)
    implementation(Deps.Spring.Core.context)
    implementation(Deps.Spring.Boot.mongodb)
}