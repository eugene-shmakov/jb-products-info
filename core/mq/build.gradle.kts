description = "Message queue"

dependencies {
    implementation(project(Projects.Domain.REPOSITORIES))
    implementation(Deps.Kotlin.stdlib)
    implementation(Deps.Spring.Boot.amqp)
}