package com.jetbrains.info.mq

import com.jetbrains.info.domain.listeners.DownloadQueueListener
import com.jetbrains.info.domain.model.ReleaseInfo
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.stereotype.Component

@Component
class QueueListener(
    private val listener: DownloadQueueListener,
) {
    @RabbitListener(queues = [MQConfig.DOWNLOADS_QUEUE_NAME], concurrency = "\${workers.count}")
    fun onMessage(release: ReleaseInfo.Key) {
        with(Thread.currentThread()) {
            name = name.replaceBeforeLast("-", "Worker")
        }
        listener.onNewReleaseAvailable(release)
    }
}