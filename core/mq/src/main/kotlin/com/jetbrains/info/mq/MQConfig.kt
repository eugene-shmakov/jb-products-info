package com.jetbrains.info.mq

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class MQConfig {
    @Bean
    fun downloads() = Queue(DOWNLOADS_QUEUE_NAME)

    companion object {
        const val DOWNLOADS_QUEUE_NAME = "jpi-downloads"
    }
}