package com.jetbrains.info.mq

import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.repository.DownloadQueue
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Component

@Component
internal class DownloadQueueImpl(
    private val rabbitTemplate: RabbitTemplate,
) : DownloadQueue {
    override fun enqueueRelease(release: ReleaseInfo) {
        rabbitTemplate.convertAndSend(MQConfig.DOWNLOADS_QUEUE_NAME, release.id)
    }
}