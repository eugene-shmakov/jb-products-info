description = "Core utils"

plugins {
    id("java-test-fixtures")
}

dependencies {
    implementation(Deps.Kotlin.stdlib)
    implementation(Deps.Spring.Data.commons)

    testFixturesApi(Deps.Test.assertJ)
    testFixturesApi(Deps.Test.mockk)
    testFixturesApi(Deps.Test.JUnit.api)
    testFixturesApi(Deps.Test.JUnit.params)

    testFixturesImplementation(Deps.Jackson.DataType.jsr310)
    testFixturesImplementation(Deps.Jackson.Module.kotlin)

    testRuntimeOnly(Deps.Test.JUnit.engine)
}