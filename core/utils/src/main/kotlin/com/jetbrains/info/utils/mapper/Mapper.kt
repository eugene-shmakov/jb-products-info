package com.jetbrains.info.utils.mapper

interface Mapper<A, B> : (A) -> B {

    fun A.mapEntity(): B

    override fun invoke(entity: A) = entity.mapEntity()
}