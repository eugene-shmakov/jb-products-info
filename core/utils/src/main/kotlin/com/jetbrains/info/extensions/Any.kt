package com.jetbrains.info.extensions

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.util.ProxyUtils

fun <T : Any> T.getLogger(): Lazy<Logger> = lazy { LoggerFactory.getLogger(javaClass.declaringClass ?: unproxiedClass) }

/**
 * Returns the runtime Java class of this object without CGLIB wrappers
 */
@Suppress("UNCHECKED_CAST")
val <T : Any> T.unproxiedClass: Class<T>?
    get() = ProxyUtils.getUserClass(this::class.java) as? Class<T>