package com.jetbrains.info.utils

import java.io.FilterInputStream
import java.io.InputStream

class ProgressInputStream(
    inputStream: InputStream,
    private val notifyEachNBytes: Long,
    private val listener: Listener,
) : FilterInputStream(inputStream) {
    var total = 0L
    var notifiedAt = 0L

    override fun read(): Int =
        super.read()
            .also { if (it != -1) increaseAndNotify(1) }

    override fun read(b: ByteArray, off: Int, len: Int): Int =
        super.read(b, off, len)
            .also { increaseAndNotify(it.toLong()) }

    override fun skip(n: Long): Long =
        super.skip(n)
            .also(::increaseAndNotify)

    private fun increaseAndNotify(read: Long) {
        total += read
        val x = total / notifyEachNBytes
        if (x != notifiedAt) {
            notifiedAt = x
            listener.onProgress(total)
        }
    }

    fun interface Listener {
        fun onProgress(read: Long)
    }
}