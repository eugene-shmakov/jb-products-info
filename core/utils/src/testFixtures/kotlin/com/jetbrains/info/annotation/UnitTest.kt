package com.jetbrains.info.annotation

import com.jetbrains.info.junit5.extensions.AssertJConfigurationExtension
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.extension.ExtendWith

@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Tag("unit")
@ExtendWith(AssertJConfigurationExtension::class)
annotation class UnitTest