package com.jetbrains.info

import com.jetbrains.info.annotation.UnitTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

@UnitTest
class InfrastructureTest {

    @Test
    fun `success test`() {
        assertThat(2 + 2).isEqualTo(4)
    }
}
