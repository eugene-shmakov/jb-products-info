package com.jetbrains.info.data.api

import com.jetbrains.info.annotation.IntegrationTest
import com.jetbrains.info.data.di.NetworkModule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@IntegrationTest
@SpringBootTest(classes = [NetworkModule::class])
internal class ProductListApiTest @Autowired constructor(
    private val api: ProductListApi,
) {
    @Test
    fun getAllProducts() {
        val result = api.getAllProducts()
        assertThat(result.getOrThrow().product).isNotEmpty
            .anyMatch { it.code.containsAll(listOf("IU", "IC")) }
    }
}