package com.jetbrains.info.data.api

import com.jetbrains.info.annotation.IntegrationTest
import com.jetbrains.info.data.di.NetworkModule
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@IntegrationTest
@SpringBootTest(classes = [NetworkModule::class])
internal class ProductApiTest @Autowired constructor(
    private val api: ProductApi,
) {
    @Test
    fun getProductInfo() {
        val result = api.getProductInfo(listOf("DS", "PD")).getOrThrow()
        assertThat(result).hasSize(2)
        assertThat(result[0]).isEqualTo(result[1])
    }
}