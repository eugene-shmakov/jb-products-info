package com.jetbrains.info.data.model.json

data class DownloadsDto(
    val linux: DownloadDetailsDto?,
)
