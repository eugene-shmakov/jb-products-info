package com.jetbrains.info.data.api

import com.jetbrains.info.data.model.json.ProductDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI

@Component
internal class ProductApiImpl @Autowired constructor(
    @Value("\${api.product-details.uri}")
    url: String,
    private val restTemplate: RestTemplate,
) : ProductApi {
    private val uriBuilder = UriComponentsBuilder.fromHttpUrl(url)

    override fun getProductInfo(codes: List<String>): Result<List<ProductDto>> = runCatching {
        val url: URI = uriBuilder.cloneBuilder().queryParam("code", codes).build(emptyMap<String, Nothing?>())
        restTemplate.exchange(
            /* url = */
            url,
            /* method = */
            HttpMethod.GET,
            /* requestEntity = */
            null,
            /* responseType = */
            object : ParameterizedTypeReference<List<ProductDto>>() {},
        ).body!!
    }
}
