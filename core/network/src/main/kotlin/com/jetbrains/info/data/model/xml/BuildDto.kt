package com.jetbrains.info.data.model.xml

data class BuildDto(
    val number: String,
    val version: String,
    val releaseDate: String?,
    val fullNumber: String?,
)
