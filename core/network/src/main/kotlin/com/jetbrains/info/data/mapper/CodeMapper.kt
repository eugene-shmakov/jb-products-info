package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.xml.ProductDto
import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.utils.mapper.Mapper

internal object CodeMapper : Mapper<ProductDto, List<Code>> {
    override fun ProductDto.mapEntity() =
        this.code.map(::Code)
}