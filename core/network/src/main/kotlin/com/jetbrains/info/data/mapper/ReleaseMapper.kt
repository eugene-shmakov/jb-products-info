package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.json.ReleaseDto
import com.jetbrains.info.domain.model.Release
import com.jetbrains.info.utils.mapper.Mapper
import java.time.LocalDate

internal object ReleaseMapper : Mapper<ReleaseDto, Release?> {
    override fun ReleaseDto.mapEntity(): Release? {
        return Release(
            date = LocalDate.parse(date),
            build = build,
            version = version,
            link = downloads.linux?.link ?: return null,
        )
    }
}
