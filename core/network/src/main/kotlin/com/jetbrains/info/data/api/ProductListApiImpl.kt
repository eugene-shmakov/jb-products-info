package com.jetbrains.info.data.api

import com.jetbrains.info.data.model.xml.ProductListDto
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate

@Component
internal class ProductListApiImpl(
    @Value("\${api.products.uri}")
    private val url: String,
    private val restTemplate: RestTemplate
) : ProductListApi {
    override fun getAllProducts(): Result<ProductListDto> = runCatching {
        restTemplate.getForEntity(url, ProductListDto::class.java).body!!
    }
}