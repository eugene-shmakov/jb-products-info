package com.jetbrains.info.data.model.json

data class DownloadDetailsDto(
    val link: String,
    val size: Long,
    val checksumLink: String,
)
