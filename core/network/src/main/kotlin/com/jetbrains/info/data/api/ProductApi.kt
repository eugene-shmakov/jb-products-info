package com.jetbrains.info.data.api

import com.jetbrains.info.data.model.json.ProductDto

interface ProductApi {
    fun getProductInfo(codes: List<String>): Result<List<ProductDto>>
}