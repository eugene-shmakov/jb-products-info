package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.json.ProductDto
import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.utils.mapper.Mapper

internal object ProductSummaryMapper : Mapper<ProductDto, ProductSummary> {
    override fun ProductDto.mapEntity() =
        ProductSummary(
            name = name,
            code = Code(code),
            releases = releases.mapNotNull(ReleaseMapper)
        )
}