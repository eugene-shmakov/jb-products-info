package com.jetbrains.info.data.model.json

data class ReleaseDto(
    val date: String,
    val version: String,
    val build: String,
    val downloads: DownloadsDto,
)
