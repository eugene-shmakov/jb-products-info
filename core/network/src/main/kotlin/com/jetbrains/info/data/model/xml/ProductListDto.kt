package com.jetbrains.info.data.model.xml

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

data class ProductListDto(
    @JacksonXmlElementWrapper(useWrapping = false)
    val product: List<ProductDto>,
)