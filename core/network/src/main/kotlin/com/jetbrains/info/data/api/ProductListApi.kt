package com.jetbrains.info.data.api

import com.jetbrains.info.data.model.xml.ProductListDto

interface ProductListApi {
    fun getAllProducts(): Result<ProductListDto>
}