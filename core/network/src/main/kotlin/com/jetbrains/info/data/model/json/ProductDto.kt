package com.jetbrains.info.data.model.json

data class ProductDto(
    val code: String,
    val name: String,
    val releases: List<ReleaseDto>,
)