package com.jetbrains.info.data.model.xml

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

data class ChannelDto(
    val id: String,
    @JacksonXmlElementWrapper(useWrapping = false)
    val build: List<BuildDto>,
)
