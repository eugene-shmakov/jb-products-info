package com.jetbrains.info.data.model.xml

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper

data class ProductDto(
    val name: String,
    @JacksonXmlElementWrapper(useWrapping = false)
    val code: List<String>,
    @JacksonXmlElementWrapper(useWrapping = false)
    val channel: List<ChannelDto>,
)