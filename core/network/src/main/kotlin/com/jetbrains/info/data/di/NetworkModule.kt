package com.jetbrains.info.data.di

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
@ComponentScan("com.jetbrains.info.data")
internal class NetworkModule {
    @Bean
    fun restTemplate(): RestTemplate = RestTemplate()
}