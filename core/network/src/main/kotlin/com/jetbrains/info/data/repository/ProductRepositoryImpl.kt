package com.jetbrains.info.data.repository

import com.jetbrains.info.data.api.ProductApi
import com.jetbrains.info.data.api.ProductListApi
import com.jetbrains.info.data.mapper.CodeMapper
import com.jetbrains.info.data.mapper.ProductSummaryMapper
import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.repository.ProductRepository
import org.springframework.stereotype.Component

@Component
internal class ProductRepositoryImpl(
    private val productListApi: ProductListApi,
    private val productApi: ProductApi,
) : ProductRepository {
    override fun getAllProductCodes(): Result<List<Code>> =
        productListApi.getAllProducts()
            .map { it.product.flatMap(CodeMapper) }

    override fun getProductSummary(codes: List<Code>): Result<List<ProductSummary>> =
        productApi.getProductInfo(codes.map { it.code })
            .map { it.map(ProductSummaryMapper) }
}