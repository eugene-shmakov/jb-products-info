description = "Network utils"

dependencies {
    implementation(project(Projects.Core.UTILS))
    implementation(project(Projects.Domain.REPOSITORIES))
    implementation(Deps.Kotlin.stdlib)
    implementation(Deps.Jackson.DataFormat.xml)
    implementation(Deps.Spring.Boot.web)

    testImplementation(testFixtures(project(Projects.Core.UTILS)))
    testImplementation(Deps.Spring.Boot.test)
}