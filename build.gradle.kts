import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import extensions.printSuiteResult
import io.gitlab.arturbosch.detekt.Detekt
import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED
import org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import utils.dependencies.type

plugins {
    id(Deps.GradlePlugins.Detekt.core) version Versions.GradlePlugins.detekt
    kotlin("jvm") version Versions.kotlin apply false
    kotlin("plugin.spring") version Versions.kotlin apply false
    id(Deps.GradlePlugins.dependencyUpdates) version Versions.GradlePlugins.dependencyUpdates
    id(Deps.GradlePlugins.springBoot) version Versions.Spring.boot apply false
}

allprojects {
    tasks {
        withType<KotlinCompile> {
            with(kotlinOptions) {
                jvmTarget = "11"
                allWarningsAsErrors = true
            }
        }
        withType<Test> {
            useJUnitPlatform()
            systemProperties = System.getProperties().mapKeys { it.key as String }
            testLogging {
                events = setOf(SKIPPED, FAILED)
            }
            afterSuite(printSuiteResult)
        }
        withType<DependencyUpdatesTask> {
            rejectVersionIf {
                "${candidate.group}:${candidate.module}" in utils.dependencies.ignoredModules ||
                        currentVersion.type > candidate.version.type
            }
        }
    }
}

subprojects {
    apply {
        plugin("kotlin")
        plugin("org.jetbrains.kotlin.plugin.spring")
        plugin(Deps.GradlePlugins.Detekt.core)
    }

    tasks.withType<Detekt> {
        allRules = true
        config.setFrom("$rootDir/detekt.yml")
        parallel = true
        basePath = rootDir.toString()
        exclude("build/")
        reports {
            html.required.set(true)
            sarif.required.set(false)
            txt.required.set(false)
            xml.required.set(false)
        }
    }

    dependencies {
        detektPlugins(Deps.GradlePlugins.Detekt.formatting)
    }
}