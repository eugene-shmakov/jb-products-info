package com.jetbrains.info.domain.listeners

import com.jetbrains.info.domain.model.ReleaseInfo

interface DownloadQueueListener {
    fun onNewReleaseAvailable(release: ReleaseInfo.Key)
}