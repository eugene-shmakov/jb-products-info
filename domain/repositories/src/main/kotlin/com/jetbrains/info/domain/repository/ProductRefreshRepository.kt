package com.jetbrains.info.domain.repository

import com.jetbrains.info.domain.model.ProductRefreshInfo

interface ProductRefreshRepository {
    fun findAll(): List<ProductRefreshInfo>

    fun save(products: List<ProductRefreshInfo>)
}
