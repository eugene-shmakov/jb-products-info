package com.jetbrains.info.domain.repository

import com.jetbrains.info.domain.model.ReleaseInfo

interface DownloadQueue {
    fun enqueueRelease(release: ReleaseInfo)
}