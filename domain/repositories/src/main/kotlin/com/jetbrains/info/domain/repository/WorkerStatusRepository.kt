package com.jetbrains.info.domain.repository

import com.jetbrains.info.domain.model.WorkerStatus

interface WorkerStatusRepository {

    fun reportStatus(workerId: String, status: WorkerStatus)

    fun removeStatus(workerId: String)

    fun getOverallStatus(): List<WorkerStatus>
}