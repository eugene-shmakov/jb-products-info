package com.jetbrains.info.domain.repository

import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ProductSummary

interface ProductRepository {
    fun getAllProductCodes(): Result<List<Code>>

    fun getProductSummary(codes: List<Code>): Result<List<ProductSummary>>
}