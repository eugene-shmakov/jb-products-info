package com.jetbrains.info.domain.repository

import com.jetbrains.info.domain.model.ReleaseInfo
import java.util.stream.Stream

interface ReleaseInfoRepository {
    fun findById(id: ReleaseInfo.Key): ReleaseInfo?

    fun findAll(): Stream<ReleaseInfo>

    fun findByCodeHavingJsonInfo(code: String): List<ReleaseInfo>

    fun findByCodeAndBuildNumber(code: String, buildNumber: String): ReleaseInfo?

    fun save(releaseInfo: ReleaseInfo)

    fun save(releases: List<ReleaseInfo>)
}
