package com.jetbrains.info.domain.model

fun ReleaseInfo.Companion.create(
    product: ReleaseInfo.Product = ReleaseInfo.Product.create(),
    release: Release = Release.create(),
    status: ReleaseInfo.Status = ReleaseInfo.Status.ENQUEUED,
    jsonInfo: String? = null,
    errorReason: String? = null,
) =
    ReleaseInfo(
        product = product,
        release = release,
        status = status,
        jsonInfo = jsonInfo,
        errorReason = errorReason,
    )

fun ReleaseInfo.Product.Companion.create(
    name: String = "DataGrip",
    code: String = "DG",
) =
    ReleaseInfo.Product(
        name = name,
        code = code
    )
