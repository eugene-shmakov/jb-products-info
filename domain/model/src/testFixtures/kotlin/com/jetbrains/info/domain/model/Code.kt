package com.jetbrains.info.domain.model

fun Code.Companion.create(
    code: String = "IU"
) =
    Code(
        code = code
    )