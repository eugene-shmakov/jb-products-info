package com.jetbrains.info.domain.model

import java.time.LocalDate

fun Release.Companion.create(
    date: LocalDate = LocalDate.now(),
    version: String = "2022.2.1",
    build: String = "222.3739.54",
    link: String = "https://download.jetbrains.com/idea/ideaIU-2022.2.1.tar.gz",
) =
    Release(
        date = date,
        version = version,
        build = build,
        link = link
    )
