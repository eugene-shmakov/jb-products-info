package com.jetbrains.info.domain.model

fun ProductSummary.Companion.create(
    name: String = "IntelliJ IDEA Ultimate",
    code: Code = Code.create(),
    releases: List<Release> = listOf(Release.create()),
) =
    ProductSummary(
        name = name,
        code = code,
        releases = releases
    )