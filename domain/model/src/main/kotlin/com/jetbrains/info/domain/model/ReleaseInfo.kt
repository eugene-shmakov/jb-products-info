package com.jetbrains.info.domain.model

import java.io.Serializable

data class ReleaseInfo(
    val product: Product,
    val release: Release,
    val status: Status,
    val jsonInfo: String?,
    val errorReason: String?,
    val id: Key = Key.of(product, release)
) {

    data class Product(
        val name: String,
        val code: String,
    ) {
        companion object {
            fun of(productSummary: ProductSummary): Product =
                Product(
                    name = productSummary.name,
                    code = productSummary.code.code,
                )
        }
    }

    data class Key(
        val productName: String,
        val buildNumber: String,
    ) : Serializable {
        companion object {
            const val serialVersionUID: Long = 1L

            fun of(product: Product, release: Release): Key =
                Key(
                    productName = product.name,
                    buildNumber = release.build,
                )
        }
    }

    enum class Status {
        ENQUEUED,
        DOWNLOADING,
        READY,
        ERROR,
    }

    companion object
}
