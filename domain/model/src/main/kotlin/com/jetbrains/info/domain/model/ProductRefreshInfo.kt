package com.jetbrains.info.domain.model

import java.time.LocalDateTime

data class ProductRefreshInfo(
    val code: String,
    val lastRefreshTime: LocalDateTime
)
