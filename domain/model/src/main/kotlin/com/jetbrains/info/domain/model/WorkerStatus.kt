package com.jetbrains.info.domain.model

sealed interface WorkerStatus {
    object Idle : WorkerStatus
    data class Downloading(
        val releaseInfo: ReleaseInfo,
        val readBytes: Long? = null,
        val totalBytes: Long? = null,
    ) : WorkerStatus
}