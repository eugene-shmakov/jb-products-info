package com.jetbrains.info.domain.model

@JvmInline
value class Code(val code: String) {
    companion object
}