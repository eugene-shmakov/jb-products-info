package com.jetbrains.info.domain.model

data class ProductSummary(
    val name: String,
    val code: Code,
    val releases: List<Release>,
) {
    companion object
}
