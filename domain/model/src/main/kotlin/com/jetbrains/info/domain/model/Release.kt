package com.jetbrains.info.domain.model

import java.time.LocalDate

data class Release(
    val date: LocalDate,
    val version: String,
    val build: String,
    val link: String,
) {
    companion object
}
