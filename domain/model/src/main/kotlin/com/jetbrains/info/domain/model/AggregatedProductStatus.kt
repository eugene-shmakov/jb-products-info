package com.jetbrains.info.domain.model

import java.time.LocalDateTime

data class AggregatedProductStatus(
    val code: Code,
    val name: String,
    val lastRefreshTime: LocalDateTime?,
    val releases: Releases,
) {
    data class Releases(
        val downloading: Int,
        val ready: Int,
        val error: Int,
        val waitingForDownload: Int,
    ) {
        val total: Int
            get() = downloading + ready + error + waitingForDownload
    }
}
