description = "Domain use cases"

dependencies {
    api(project(Projects.Domain.MODEL))
    implementation(project(Projects.Core.UTILS))
    implementation(project(Projects.Domain.REPOSITORIES))
    implementation(Deps.Kotlin.stdlib)
    implementation(Deps.Kotlin.stdlibJdk8)
    implementation(Deps.Spring.Core.context)
    implementation(Deps.Spring.Data.commons)

    testImplementation(testFixtures(project(Projects.Core.UTILS)))
    testImplementation(testFixtures(project(Projects.Domain.MODEL)))
}