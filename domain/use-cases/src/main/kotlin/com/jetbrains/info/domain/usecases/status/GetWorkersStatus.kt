package com.jetbrains.info.domain.usecases.status

import com.jetbrains.info.domain.model.WorkerStatus
import com.jetbrains.info.domain.repository.WorkerStatusRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class GetWorkersStatus(
    private val repository: WorkerStatusRepository,
    @Value("\${workers.count}")
    private val workersCount: Int,
) {

    operator fun invoke(): List<WorkerStatus> =
        repository.getOverallStatus().asSequence()
            .plus(generateSequence { WorkerStatus.Idle })
            .take(workersCount)
            .toList()
}
