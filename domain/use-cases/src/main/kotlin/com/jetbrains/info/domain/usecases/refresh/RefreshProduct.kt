package com.jetbrains.info.domain.usecases.refresh

import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.repository.ProductRepository
import com.jetbrains.info.domain.usecases.FilterProducts
import org.springframework.stereotype.Component

@Component
class RefreshProduct(
    private val productRepository: ProductRepository,
    private val updateLastRefreshTime: UpdateLastRefreshTime,
    private val filterProducts: FilterProducts
) {
    operator fun invoke(code: Code): Result<ProductSummary?> = runCatching {
        val productSummary = productRepository.getProductSummary(listOf(code)).getOrThrow()
        updateLastRefreshTime(productSummary.map { it.code })
        productSummary
            .let(filterProducts::invoke)
            .firstOrNull()
    }
}