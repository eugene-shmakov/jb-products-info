package com.jetbrains.info.domain.usecases.storage

import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import org.springframework.stereotype.Component

@Component
class StoreReleaseInfo(
    private val repository: ReleaseInfoRepository
) {

    operator fun invoke(releaseInfo: ReleaseInfo) {
        repository.save(releaseInfo)
    }
}
