package com.jetbrains.info.domain.usecases.refresh

import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ProductRefreshInfo
import com.jetbrains.info.domain.repository.ProductRefreshRepository
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class UpdateLastRefreshTime(
    private val productRefreshRepository: ProductRefreshRepository,
) {

    operator fun invoke(codes: List<Code>) {
        val now = LocalDateTime.now()
        productRefreshRepository.save(
            codes.map {
                ProductRefreshInfo(
                    code = it.code,
                    lastRefreshTime = now,
                )
            }
        )
    }
}