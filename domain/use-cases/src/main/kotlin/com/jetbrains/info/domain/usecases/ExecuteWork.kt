package com.jetbrains.info.domain.usecases

import com.jetbrains.info.domain.model.WorkerStatus
import com.jetbrains.info.domain.repository.WorkerStatusRepository
import org.springframework.stereotype.Component
import java.util.*

@Component
class ExecuteWork(
    private val repository: WorkerStatusRepository
) {

    operator fun invoke(block: StatusReporter.() -> Unit) {
        RepositoryWrapperImpl().use(block)
    }

    interface StatusReporter {
        fun reportStatus(status: WorkerStatus)
    }

    private inner class RepositoryWrapperImpl : StatusReporter, AutoCloseable {

        val workerId: String = UUID.randomUUID().toString()

        override fun reportStatus(status: WorkerStatus) {
            repository.reportStatus(workerId, status)
        }

        override fun close() {
            repository.removeStatus(workerId = workerId)
        }
    }
}