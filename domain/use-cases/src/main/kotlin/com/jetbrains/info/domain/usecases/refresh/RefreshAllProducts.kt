package com.jetbrains.info.domain.usecases.refresh

import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.repository.ProductRepository
import com.jetbrains.info.domain.usecases.FilterProducts
import org.springframework.stereotype.Component

@Component
class RefreshAllProducts(
    private val productRepository: ProductRepository,
    private val updateLastRefreshTime: UpdateLastRefreshTime,
    private val filterProducts: FilterProducts
) {

    operator fun invoke(): Result<List<ProductSummary>> = runCatching {
        val codes = productRepository.getAllProductCodes().getOrThrow()
        val productsSummary = productRepository.getProductSummary(codes).getOrThrow()
        updateLastRefreshTime(productsSummary.map { it.code })
        productsSummary
            .distinct()
            .let(filterProducts::invoke)
    }
}