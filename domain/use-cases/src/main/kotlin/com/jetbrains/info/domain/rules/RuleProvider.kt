package com.jetbrains.info.domain.rules

import org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Scope
import java.time.LocalDate
import java.time.Period

@Configuration
class RuleProvider {

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    fun releaseFilteringRule(): ReleaseFilteringRule =
        ReleaseAgeFilteringRule(
            today = LocalDate.now(),
            age = Period.ofYears(1),
        )
}