package com.jetbrains.info.domain.usecases

import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.rules.ReleaseFilteringRule
import org.springframework.beans.factory.annotation.Lookup
import org.springframework.stereotype.Component

@Component
abstract class FilterProducts {
    operator fun invoke(products: List<ProductSummary>): List<ProductSummary> {
        val releaseFilteringRule = getReleaseFilteringRule()
        return products.mapNotNull { product ->
            val releases = product.releases.filter(releaseFilteringRule).ifEmpty { return@mapNotNull null }
            product.copy(releases = releases)
        }
    }

    @Lookup
    abstract fun getReleaseFilteringRule(): ReleaseFilteringRule
}