package com.jetbrains.info.domain.usecases.storage

import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.model.ReleaseInfo.Status.*
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import com.jetbrains.info.extensions.getLogger
import org.springframework.stereotype.Component

@Component
class GetAndUpdateEnqueuedReleaseInfo(
    private val repository: ReleaseInfoRepository
) {
    private val log by getLogger()

    operator fun invoke(id: ReleaseInfo.Key): ReleaseInfo? {
        val releaseInfo = repository.findById(id) ?: return null
        when (releaseInfo.status) {
            ENQUEUED, ERROR -> Unit
            DOWNLOADING -> log.warn("release(id=$id') is already downloading, download it once again")
            READY -> {
                log.warn("release(id=$id') is already downloaded. Ignoring this request")
                return null
            }
        }
        return releaseInfo.copy(status = DOWNLOADING, errorReason = null)
            .also(repository::save)
    }
}
