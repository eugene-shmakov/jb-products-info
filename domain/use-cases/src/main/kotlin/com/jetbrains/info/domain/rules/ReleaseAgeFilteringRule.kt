package com.jetbrains.info.domain.rules

import com.jetbrains.info.domain.model.Release
import java.time.LocalDate
import java.time.temporal.TemporalAmount

class ReleaseAgeFilteringRule(
    private val today: LocalDate,
    private val age: TemporalAmount,
) : ReleaseFilteringRule {
    override fun invoke(release: Release): Boolean =
        today.minus(age) <= release.date
}