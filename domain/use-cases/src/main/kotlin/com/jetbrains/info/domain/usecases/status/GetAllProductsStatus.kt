package com.jetbrains.info.domain.usecases.status

import com.jetbrains.info.domain.model.AggregatedProductStatus
import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ReleaseInfo.Status.*
import com.jetbrains.info.domain.repository.ProductRefreshRepository
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import org.springframework.stereotype.Component
import kotlin.streams.asSequence

@Component
class GetAllProductsStatus(
    private val releaseInfoRepository: ReleaseInfoRepository,
    private val productRefreshRepository: ProductRefreshRepository,
) {

    operator fun invoke(): List<AggregatedProductStatus> {
        val lastRefreshTime = productRefreshRepository.findAll().associate { Code(it.code) to it.lastRefreshTime }
        return releaseInfoRepository.findAll().use { stream ->
            val names = mutableMapOf<Code, String>()
            stream.asSequence()
                .onEach { names[Code(it.product.code)] = it.product.name }
                .groupingBy { Code(it.product.code) }
                .aggregate { _, acc: AggregatedProductStatus.Releases?, release, _ ->
                    val current = acc ?: EMPTY
                    when (release.status) {
                        ENQUEUED -> current.copy(waitingForDownload = current.waitingForDownload + 1)
                        DOWNLOADING -> current.copy(downloading = current.downloading + 1)
                        READY -> current.copy(ready = current.ready + 1)
                        ERROR -> current.copy(error = current.error + 1)
                    }
                }
                .entries
                .map { (code, releases) ->
                    AggregatedProductStatus(
                        code = code,
                        name = names[code].orEmpty(),
                        lastRefreshTime = lastRefreshTime[code],
                        releases = releases,
                    )
                }
                .sortedBy { it.code.code }
        }
    }

    companion object {
        private val EMPTY =
            AggregatedProductStatus.Releases(
                downloading = 0,
                ready = 0,
                error = 0,
                waitingForDownload = 0,
            )
    }
}