package com.jetbrains.info.domain.usecases.refresh

import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.model.ReleaseInfo.Product
import com.jetbrains.info.domain.model.ReleaseInfo.Status.ERROR
import com.jetbrains.info.domain.repository.DownloadQueue
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import org.springframework.stereotype.Component
import kotlin.streams.asSequence

@Component
class EnqueueNewReleases(
    private val repository: ReleaseInfoRepository,
    private val downloadQueue: DownloadQueue,
) {

    operator fun invoke(products: List<ProductSummary>): Int {
        val existing = repository.findAll().use { stream ->
            stream.asSequence()
                .filter { it.status != ERROR }
                .map { it.id }
                .toSet()
        }

        val newReleases = products.asSequence()
            .flatMap { product ->
                product.releases.asSequence().map { release ->
                    ReleaseInfo(
                        product = Product.of(product),
                        release = release,
                        status = ReleaseInfo.Status.ENQUEUED,
                        jsonInfo = null,
                        errorReason = null,
                    )
                }
            }
            .filter { it.id !in existing }
            .toList()

        repository.save(newReleases)
        newReleases.forEach(downloadQueue::enqueueRelease)
        return newReleases.size
    }
}