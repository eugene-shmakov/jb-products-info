package com.jetbrains.info.domain.rules

import com.jetbrains.info.domain.model.Release

interface ReleaseFilteringRule : (Release) -> Boolean