package com.jetbrains.info.domain.usecases.status

import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import org.springframework.stereotype.Component

@Component
class GetAllProductReleases(
    private val repository: ReleaseInfoRepository,
) {

    operator fun invoke(code: Code): List<ReleaseInfo> =
        repository.findByCodeHavingJsonInfo(code.code)
}