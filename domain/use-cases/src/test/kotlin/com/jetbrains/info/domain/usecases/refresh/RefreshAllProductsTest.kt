package com.jetbrains.info.domain.usecases.refresh

import com.jetbrains.info.annotation.UnitTest
import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.model.Release
import com.jetbrains.info.domain.model.create
import com.jetbrains.info.domain.repository.ProductRepository
import com.jetbrains.info.domain.rules.ReleaseAgeFilteringRule
import com.jetbrains.info.domain.usecases.FilterProducts
import io.mockk.every
import io.mockk.mockk
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.time.LocalDate
import java.time.Period
import java.time.temporal.TemporalAmount

@UnitTest
internal class RefreshAllProductsTest {
    private val repository = mockk<ProductRepository> {
        every { getAllProductCodes() } returns Result.success(codes)
        every { getProductSummary(codes) } returns Result.success(products)
    }

    @ParameterizedTest
    @MethodSource("cases")
    fun test(case: TestCase) {
        val refreshAllProducts = RefreshAllProducts(
            productRepository = repository,
            updateLastRefreshTime = mockk(relaxed = true),
            filterProducts = object : FilterProducts() {
                override fun getReleaseFilteringRule() =
                    ReleaseAgeFilteringRule(today = case.today, age = case.age)
            }
        )

        val result = refreshAllProducts()
        assertThat(result.getOrThrow()).containsExactlyInAnyOrderElementsOf(case.expected)
    }

    data class TestCase(
        val today: LocalDate,
        val age: TemporalAmount = Period.ofYears(1),
        val expected: List<ProductSummary>,
    )

    companion object {
        private val codes = listOf(Code("OC"))

        private val appCode = ProductSummary.create(name = "AppCode", releases = emptyList())
        private val idea = ProductSummary.create(
            releases = listOf(
                Release.create(date = LocalDate.of(2022, 8, 15)),
                Release.create(date = LocalDate.of(2022, 7, 14)),
                Release.create(date = LocalDate.of(2021, 6, 13)),
            )
        )
        private val dataSpell = ProductSummary.create(
            name = "DataSpell",
            releases = listOf(
                Release.create(date = LocalDate.of(2022, 1, 12))
            )
        )
        private val products = listOf(appCode, dataSpell, idea, dataSpell)

        @JvmStatic
        fun cases() = listOf(
            TestCase(today = LocalDate.of(2024, 1, 1), expected = emptyList()),
            TestCase(
                today = LocalDate.of(2023, 7, 15),
                expected = listOf(idea.copy(releases = idea.releases.take(1)))
            ),
            TestCase(
                today = LocalDate.of(2023, 7, 14),
                expected = listOf(idea.copy(releases = idea.releases.take(2)))
            ),
            TestCase(
                today = LocalDate.of(2022, 8, 28),
                expected = listOf(
                    idea.copy(releases = idea.releases.take(2)),
                    dataSpell,
                )
            ),
            TestCase(
                today = LocalDate.of(2022, 6, 13),
                expected = listOf(idea, dataSpell)
            ),
        )
    }
}