package com.jetbrains.info.domain.usecases.refresh

import com.jetbrains.info.domain.model.ProductSummary
import com.jetbrains.info.domain.model.Release
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.model.ReleaseInfo.Status.*
import com.jetbrains.info.domain.model.create
import com.jetbrains.info.domain.repository.DownloadQueue
import com.jetbrains.info.domain.repository.ReleaseInfoRepository
import io.mockk.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

internal class EnqueueNewReleasesTest {
    private val downloadQueue = mockk<DownloadQueue>(relaxed = true)
    private val repository = mockk<ReleaseInfoRepository>()
    private val enqueueNewReleases = EnqueueNewReleases(repository, downloadQueue)

    @ParameterizedTest
    @MethodSource("cases")
    fun test(case: TestCase) {
        val slot = slot<List<ReleaseInfo>>()
        every { repository.findAll() } answers { case.existing.stream() }
        every { repository.save(capture(slot)) } just runs
        enqueueNewReleases(case.products)

        if (case.expected.isNotEmpty()) {
            verifySequence {
                case.expected.forEach(downloadQueue::enqueueRelease)
            }
            assertThat(slot.captured).containsExactlyInAnyOrderElementsOf(case.expected)
        }
        confirmVerified(downloadQueue)
    }

    data class TestCase(
        val products: List<ProductSummary> = listOf(product),
        val existing: List<ReleaseInfo>,
        val expected: List<ReleaseInfo>,
    )

    companion object {
        private val releases = listOf(
            Release.create(build = "1"),
            Release.create(build = "2"),
        )
        private val product = ProductSummary.create(releases = releases)
        private val releaseInfo = releases.map {
            ReleaseInfo.create(
                product = ReleaseInfo.Product.of(product),
                release = it,
                status = ENQUEUED,
            )
        }

        @JvmStatic
        fun cases() = listOf(
            TestCase(products = emptyList(), existing = emptyList(), expected = emptyList()),
            TestCase(existing = emptyList(), expected = releaseInfo),
            TestCase(existing = releaseInfo, expected = emptyList()),
            TestCase(
                existing = listOf(releaseInfo[0]),
                expected = listOf(releaseInfo[1]),
            ),
            TestCase(
                existing = listOf(releaseInfo[1]),
                expected = listOf(releaseInfo[0]),
            ),
            TestCase(
                existing = listOf(releaseInfo[1].copy(status = READY)),
                expected = listOf(releaseInfo[0]),
            ),
            TestCase(
                existing = listOf(releaseInfo[1].copy(status = DOWNLOADING)),
                expected = listOf(releaseInfo[0]),
            ),
            TestCase(
                existing = listOf(releaseInfo[1].copy(status = ERROR)),
                expected = releaseInfo,
            ),
        )
    }
}