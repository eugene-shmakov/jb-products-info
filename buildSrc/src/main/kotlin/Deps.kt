object Deps {
    object ApacheCommons {
        const val compress = "org.apache.commons:commons-compress:${Versions.apacheCommonsCompress}"
    }

    object GradlePlugins {
        const val springBoot = "org.springframework.boot"
        const val dependencyUpdates = "com.github.ben-manes.versions"

        object Detekt {
            const val core = "io.gitlab.arturbosch.detekt"
            const val formatting = "$core:detekt-formatting:${Versions.GradlePlugins.detekt}"
        }
    }

    object Jackson {

        object DataType {
            const val jsr310 = "com.fasterxml.jackson.datatype:jackson-datatype-jsr310:${Versions.jackson}"
        }

        object DataFormat {
            const val xml = "com.fasterxml.jackson.dataformat:jackson-dataformat-xml:${Versions.jackson}"
        }

        object Module {
            const val kotlin = "com.fasterxml.jackson.module:jackson-module-kotlin:${Versions.jackson}"
        }
    }

    object Kotlin {
        private const val prefix = "org.jetbrains.kotlin:kotlin"

        const val stdlib = "$prefix-stdlib:${Versions.kotlin}"
        const val stdlibJdk8 = "$prefix-stdlib-jdk8:${Versions.kotlin}"
    }

    object OpenAPI {
        const val ui = "org.springdoc:springdoc-openapi-ui:${Versions.springdoc}"
        const val kotlin = "org.springdoc:springdoc-openapi-kotlin:${Versions.springdoc}"
    }

    object Spring {
        object Boot {
            private const val prefix = "org.springframework.boot:spring-boot-starter"

            const val amqp = "$prefix-amqp:${Versions.Spring.boot}"
            const val mongodb = "$prefix-data-mongodb:${Versions.Spring.boot}"
            const val test = "$prefix-test:${Versions.Spring.boot}"
            const val web = "$prefix-web:${Versions.Spring.boot}"
        }

        object Core {
            private const val prefix = "org.springframework:spring"

            const val context = "$prefix-context:${Versions.Spring.core}"
        }

        object Data {
            private const val prefix = "org.springframework.data:spring-data"

            const val commons = "$prefix-commons:${Versions.Spring.data}"
        }
    }

    object Test {
        object JUnit {
            private const val jupiterPrefix = "org.junit.jupiter:junit-jupiter"

            const val api = "$jupiterPrefix-api:${Versions.Test.JUnit.jupiter}"
            const val engine = "$jupiterPrefix-engine:${Versions.Test.JUnit.jupiter}"
            const val params = "$jupiterPrefix-params:${Versions.Test.JUnit.jupiter}"
        }

        const val assertJ = "org.assertj:assertj-core:${Versions.Test.assertJ}"
        const val mockk = "io.mockk:mockk:${Versions.Test.mockk}"
    }
}