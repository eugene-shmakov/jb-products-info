object Projects {
    object Core {
        const val DB = ":core:db"
        const val MQ = ":core:mq"
        const val NETWORK = ":core:network"
        const val UTILS = ":core:utils"
        const val WORKER = ":core:worker"
    }

    object Domain{
        const val MODEL = ":domain:model"
        const val REPOSITORIES = ":domain:repositories"
        const val USE_CASES = ":domain:use-cases"
    }

    const val REST_SERVICE = ":rest-service"
}