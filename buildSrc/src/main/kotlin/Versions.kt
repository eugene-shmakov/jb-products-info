object Versions {
    const val kotlin = "1.7.10"
    const val apacheCommonsCompress = "1.21"
    const val jackson = "2.13.3"
    const val springdoc = "1.6.11"

    object GradlePlugins {
        const val dependencyUpdates = "0.42.0"
        const val detekt = "1.21.0"
    }

    object Spring {
        const val boot = "2.7.3"
        const val core = "5.3.22"
        const val data = "2.7.2"
    }

    object Test {
        object JUnit {
            const val jupiter = "5.9.0"
        }

        const val assertJ = "3.23.1"
        const val mockk = "1.12.7"
    }
}