package extensions

import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.TestDescriptor
import org.gradle.api.tasks.testing.TestResult
import org.gradle.kotlin.dsl.KotlinClosure2

@Suppress("unused")
val Test.printSuiteResult: KotlinClosure2<TestDescriptor, TestResult, Void>
    get() = KotlinClosure2({ desc, result ->
        if (desc.parent == null) {
            val name = desc.name
            val border = "-".repeat(80)
            val padding = border.substring(0, 40 - name.length / 2)
            with(result) {
                println(
                    """
                    $padding$name$padding
                    Results: $resultType ($testCount tests, $successfulTestCount successes, $failedTestCount failures, $skippedTestCount skipped)
                    $border
                """.trimIndent()
                )
            }
        }
        null
    })