package utils.dependencies

enum class VersionType : Comparable<VersionType> {
    ALPHA,
    BETA,
    RC,
    STABLE,
    UNKNOWN,
}