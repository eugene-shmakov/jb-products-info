FROM gradle:7.5.1-jdk11-alpine as build
WORKDIR /app
COPY . /app/
RUN gradle --no-daemon :rest-service:bootJar

FROM openjdk:11.0.12-slim
COPY --from=build /app/rest-service/build/libs/rest-service.jar /app/service.jar

ENTRYPOINT java -jar /app/service.jar
