## Description

This project contains a web service that collects `product-info.json` files from JetBrains product distributions.
![](docs/res/screenshot.png)

## Requirements

To build and run this project you will need only:

* Docker with Compose V2 support (`docker compose`)

Compose V1 (`docker-compose`) will probably work too, but the project was not tested with it.

## Build & Run

1. Clone this repo
2. Open a terminal in the root directory of the project.
3. Execute `docker compose up -d`. Wait for command to complete to build the service and run it together with its
   dependencies.
4. Execute `docker compose ps`. Look for **jpi-service** and get the port bound to **7000/tcp**\
   **Example:**

```
SERVICE       STATUS    PORTS
jpi-service   running   0.0.0.0:49434->7000/tcp
```

5. Open `http://127.0.0.1:<port>` page in a browser, e.g. http://127.0.0.1:49434 for the example above.
6. Click 'Swagger UI' link in the page footer.
7. Use Swagger UI to interact with the service API.

### Other operations

* `docker compose ps` return current status of the service and its dependencies.
* `docker compose stop` stops the service with its dependencies. Keeps the service state in the persistent storage.
* `docker compose start` starts the service with its dependencies after stopping. The service will continue its activity
  from the state it was stopped at.
* `docker compose down` stops the service with its dependencies and cleans the service state stored in the persistent
  storage.
* `docker compose up -d --scale jpi-service=2` starts two instances of the service. You may specify any number of
  instances.

### Environment variables

To configure service behavior environments variables are used.\
Copy [.env.example](.env.example) file content to a new **.env** file and run `docker compose up -d` to re-create the
service with new parameters.

The following environment variables are supported:

* `WORKERS_COUNT` - number of workers per one service instance, default value: 3.
* `UPDATES_XML_URL` URL of the initial XML file, default value: https://www.jetbrains.com/updates/updates.xml
* `PRODUCT_INFO_URL` URL of the product info API, default value: https://data.services.jetbrains.com/products

