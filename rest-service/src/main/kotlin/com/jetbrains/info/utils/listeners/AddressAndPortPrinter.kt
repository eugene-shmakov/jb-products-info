package com.jetbrains.info.utils.listeners

import com.jetbrains.info.extensions.getLogger
import org.springframework.boot.web.servlet.context.ServletWebServerInitializedEvent
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component

@Component
internal class AddressAndPortPrinter : ApplicationListener<ServletWebServerInitializedEvent> {

    private val log by getLogger()

    override fun onApplicationEvent(event: ServletWebServerInitializedEvent) {
        log.debug("Service is ready at 'http://127.0.0.1:${event.webServer.port}'")
    }
}