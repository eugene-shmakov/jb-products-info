package com.jetbrains.info.domain.model

data class RefreshStatus(
    val products: List<ProductSummary>,
    val newReleases: Int,
)
