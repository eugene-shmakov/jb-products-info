package com.jetbrains.info.domain.model

data class ServiceStatus(
    val workers: List<WorkerStatus>,
    val products: List<AggregatedProductStatus>,
)
