package com.jetbrains.info.web

import io.swagger.v3.oas.annotations.Operation
import org.springframework.core.io.ClassPathResource
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class ContentController {

    @Operation(hidden = true)
    @GetMapping("/")
    fun getStatusPage() =
        getFile("index.html")

    @Operation(hidden = true)
    @GetMapping("/content/static/{filename}")
    fun getFile(@PathVariable filename: String) =
        ClassPathResource("static/$filename").inputStream.bufferedReader().readText()
}