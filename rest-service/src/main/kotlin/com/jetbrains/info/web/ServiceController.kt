package com.jetbrains.info.web

import com.jetbrains.info.data.mapper.BuildInfoMapper
import com.jetbrains.info.data.mapper.RefreshStatusMapper
import com.jetbrains.info.data.mapper.ServiceStatusMapper
import com.jetbrains.info.data.model.BuildInfoDto
import com.jetbrains.info.data.model.RefreshStatusDto
import com.jetbrains.info.data.model.ServiceStatusDto
import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.service.ProductInfoService
import io.swagger.v3.oas.annotations.Operation
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

@RestController
class ServiceController(
    private val service: ProductInfoService,
    private val buildInfoMapper: BuildInfoMapper,
) {
    @Operation(summary = "Get a service status summary")
    @GetMapping("/status")
    fun getStatus(): ServiceStatusDto =
        service.getStatus().let(ServiceStatusMapper)

    @Operation(summary = "Get all processed builds of the selected product")
    @GetMapping("/{productCode}")
    fun getProductInfo(
        @PathVariable productCode: String
    ): List<BuildInfoDto> =
        service.getProductInfo(Code(productCode)).map(buildInfoMapper)

    @Operation(summary = "Get the product-info.json of the selected product and build")
    @GetMapping("/{productCode}/{buildNumber}")
    fun getBuildInfo(
        @PathVariable productCode: String,
        @PathVariable buildNumber: String,
    ): Map<String, Any> =
        service.getReleaseInfo(Code(productCode), buildNumber)?.let(buildInfoMapper)?.productInfo
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND)

    @Operation(summary = "Refresh all products information and schedule downloading of new available builds")
    @PostMapping("/refresh")
    fun refresh(): RefreshStatusDto =
        service.refreshAll().let(RefreshStatusMapper)

    @Operation(summary = "Refresh the selected product information and schedule downloading of new available builds")
    @PostMapping("/refresh/{productCode}")
    fun refreshProduct(
        @PathVariable productCode: String
    ): RefreshStatusDto =
        service.refresh(Code(productCode)).let(RefreshStatusMapper)
}