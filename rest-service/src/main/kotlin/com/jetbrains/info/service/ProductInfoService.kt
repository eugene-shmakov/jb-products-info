package com.jetbrains.info.service

import com.jetbrains.info.domain.model.Code
import com.jetbrains.info.domain.model.RefreshStatus
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.domain.model.ServiceStatus

interface ProductInfoService {
    fun refreshAll(): RefreshStatus
    fun refresh(code: Code): RefreshStatus
    fun getStatus(): ServiceStatus
    fun getProductInfo(code: Code): List<ReleaseInfo>
    fun getReleaseInfo(code: Code, buildNumber: String): ReleaseInfo?
}