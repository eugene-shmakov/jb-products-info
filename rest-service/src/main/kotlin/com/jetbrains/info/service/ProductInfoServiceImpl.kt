package com.jetbrains.info.service

import com.jetbrains.info.domain.model.*
import com.jetbrains.info.domain.usecases.refresh.EnqueueNewReleases
import com.jetbrains.info.domain.usecases.refresh.RefreshAllProducts
import com.jetbrains.info.domain.usecases.refresh.RefreshProduct
import com.jetbrains.info.domain.usecases.status.GetAllProductReleases
import com.jetbrains.info.domain.usecases.status.GetAllProductsStatus
import com.jetbrains.info.domain.usecases.status.GetProductReleaseInfo
import com.jetbrains.info.domain.usecases.status.GetWorkersStatus
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@EnableScheduling
@Component
class ProductInfoServiceImpl(
    private val refreshAllProducts: RefreshAllProducts,
    private val refreshProduct: RefreshProduct,
    private val enqueueNewReleases: EnqueueNewReleases,
    private val getWorkersStatus: GetWorkersStatus,
    private val getAllProductsStatus: GetAllProductsStatus,
    private val getAllProductReleases: GetAllProductReleases,
    private val getProductReleaseInfo: GetProductReleaseInfo,
) : ProductInfoService {
    @Scheduled(cron = "0 0 * * * *")
    override fun refreshAll(): RefreshStatus {
        val products = refreshAllProducts().getOrNull().orEmpty()
        return refreshProducts(products)
    }

    override fun refresh(code: Code): RefreshStatus {
        val product = refreshProduct(code).getOrNull()
        return refreshProducts(listOfNotNull(product))
    }

    override fun getStatus(): ServiceStatus =
        ServiceStatus(
            workers = getWorkersStatus(),
            products = getAllProductsStatus(),
        )

    private fun refreshProducts(products: List<ProductSummary>): RefreshStatus {
        val newReleases = enqueueNewReleases(products)

        return RefreshStatus(
            products = products,
            newReleases = newReleases,
        )
    }

    override fun getProductInfo(code: Code): List<ReleaseInfo> =
        getAllProductReleases(code)

    override fun getReleaseInfo(code: Code, buildNumber: String): ReleaseInfo? =
        getProductReleaseInfo(code = code, buildNumber = buildNumber)
}