package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.ServiceStatusDto
import com.jetbrains.info.data.model.ServiceStatusDto.Worker.Status.Downloading
import com.jetbrains.info.data.model.ServiceStatusDto.Worker.Status.Idle
import com.jetbrains.info.domain.model.WorkerStatus
import com.jetbrains.info.utils.mapper.Mapper

object WorkerMapper : Mapper<WorkerStatus, ServiceStatusDto.Worker> {
    override fun WorkerStatus.mapEntity(): ServiceStatusDto.Worker = when (this) {
        WorkerStatus.Idle ->
            ServiceStatusDto.Worker(status = Idle)

        is WorkerStatus.Downloading ->
            ServiceStatusDto.Worker(
                status = Downloading,
                link = releaseInfo.release.link,
                readBytes = readBytes,
                totalBytes = totalBytes,
                progress = "${readBytes.mb} of ${totalBytes.mb}"
            )
    }

    private const val MB = 1024 * 1024

    private val Long?.mb: String
        get() = this?.let { "${it / MB}MB" } ?: "unknown"
}
