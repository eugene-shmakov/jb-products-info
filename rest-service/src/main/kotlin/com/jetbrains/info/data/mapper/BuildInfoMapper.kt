package com.jetbrains.info.data.mapper

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.jetbrains.info.data.model.BuildInfoDto
import com.jetbrains.info.domain.model.ReleaseInfo
import com.jetbrains.info.utils.mapper.Mapper
import org.springframework.stereotype.Component

@Component
class BuildInfoMapper(
    private val jacksonMapper: ObjectMapper,
) : Mapper<ReleaseInfo, BuildInfoDto> {
    override fun ReleaseInfo.mapEntity() =
        BuildInfoDto(
            buildNumber = release.build,
            productInfo = jacksonMapper.readValue(jsonInfo ?: "{}"),
        )
}