package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.RefreshStatusDto
import com.jetbrains.info.domain.model.RefreshStatus
import com.jetbrains.info.utils.mapper.Mapper

object RefreshStatusMapper : Mapper<RefreshStatus, RefreshStatusDto> {
    override fun RefreshStatus.mapEntity() =
        RefreshStatusDto(
            products = products.size,
            totalReleases = products.sumOf { it.releases.size },
            newReleases = newReleases,
        )
}