package com.jetbrains.info.data.model

data class RefreshStatusDto(
    val products: Int,
    val totalReleases: Int,
    val newReleases: Int,
)
