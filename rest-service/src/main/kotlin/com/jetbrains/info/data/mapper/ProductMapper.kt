package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.ServiceStatusDto
import com.jetbrains.info.domain.model.AggregatedProductStatus
import com.jetbrains.info.utils.mapper.Mapper
import java.time.format.DateTimeFormatter

object ProductMapper : Mapper<AggregatedProductStatus, ServiceStatusDto.Product> {
    private val timeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

    override fun AggregatedProductStatus.mapEntity() =
        ServiceStatusDto.Product(
            code = code.code,
            name = name,
            lastRefreshTime = lastRefreshTime?.let(timeFormatter::format)
                ?: "unknown",
            releases = ServiceStatusDto.Releases(
                total = releases.total,
                downloading = releases.downloading,
                waitingForDownload = releases.waitingForDownload,
                ready = releases.ready,
                error = releases.error,
            )
        )
}
