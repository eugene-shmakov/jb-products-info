package com.jetbrains.info.data.model

import com.fasterxml.jackson.annotation.JsonInclude

data class ServiceStatusDto(
    val workers: List<Worker>,
    val summary: Summary,
    val products: List<Product>,
) {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    data class Worker(
        val status: Status,
        val link: String? = null,
        val totalBytes: Long? = null,
        val readBytes: Long? = null,
        val progress: String? = null,
    ) {
        enum class Status {
            Idle,
            Downloading,
        }
    }

    data class Summary(
        val productsCount: Int,
        val releases: Releases,
    )

    data class Product(
        val code: String,
        val name: String,
        val lastRefreshTime: String,
        val releases: Releases,
    )

    data class Releases(
        val total: Int,
        val downloading: Int,
        val ready: Int,
        val error: Int,
        val waitingForDownload: Int,
    )
}
