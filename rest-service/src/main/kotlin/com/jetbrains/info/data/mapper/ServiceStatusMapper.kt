package com.jetbrains.info.data.mapper

import com.jetbrains.info.data.model.ServiceStatusDto
import com.jetbrains.info.domain.model.ServiceStatus
import com.jetbrains.info.utils.mapper.Mapper

object ServiceStatusMapper : Mapper<ServiceStatus, ServiceStatusDto> {
    override fun ServiceStatus.mapEntity() =
        ServiceStatusDto(
            workers = workers.map(WorkerMapper),
            products = products.map(ProductMapper),
            summary = ServiceStatusDto.Summary(
                productsCount = products.size,
                releases = ServiceStatusDto.Releases(
                    total = products.sumOf { it.releases.total },
                    ready = products.sumOf { it.releases.ready },
                    error = products.sumOf { it.releases.error },
                    downloading = products.sumOf { it.releases.downloading },
                    waitingForDownload = products.sumOf { it.releases.waitingForDownload },
                )
            )
        )
}