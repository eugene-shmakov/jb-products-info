package com.jetbrains.info.data.model

import com.fasterxml.jackson.annotation.JsonProperty

data class BuildInfoDto(
    @JsonProperty("build-number")
    val buildNumber: String,
    @JsonProperty("product-info")
    val productInfo: Map<String, Any>,
)
