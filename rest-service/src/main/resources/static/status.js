function getStatus(success, error) {
    $.ajax(
        {
            url: '/status',
            type: 'GET',
            success: success,
            error: error,
        }
    );
}

function updateStatus() {
    getStatus(
        (data) => {
            renderWorkers(data.workers)
            renderProducts(data)
            setTimeout(updateStatus, 1000)
        },
        (data) => {
            $('#error').addClass("visible")
        }
    );
}

function renderProducts(data) {
    $('#products').empty()
    renderSummaryRow(data.summary)
    data.products.forEach(renderProduct)
}

function renderSummaryRow(data) {
    $('#products').append(`
    <tr>
        <td class="name"><b>Total</b></td>>
        <td></td>
        <td></td>
        ${renderReleases(data.releases)}
    </tr>
    `)
}

function renderProduct(data) {
    $('#products').append(`
    <tr>
        <td class="name">${data.name}</td>>
        <td>${data.code}</td>
        <td>${data.lastRefreshTime}</td>
        ${renderReleases(data.releases)}
    </tr>
    `)
}

function renderReleases(data) {
    return `
        <td>${data.total}</td>
        <td>${data.downloading}</td>
        <td>${data.ready}</td>
        <td>${data.error}</td>
        <td>${data.waitingForDownload}</td>
    `
}

function renderWorkers(workers) {
    $('#workers').empty()
    workers.forEach(renderWorker)
}

function renderWorker(worker, index) {
    if (worker.status === "Idle")
        $('#workers').append(`<div class="worker-label">Worker #${index + 1}: Idle</div>`)
    else if (worker.status === "Downloading")
        $('#workers').append(`
        <div class="worker-label">Worker #${index + 1}: 
            <a href="${worker.link}">${worker.link}</a>
        </div>
        <progress 
            class="worker-progress"
            max="${worker.totalBytes}"
            value="${worker.readBytes}"
            data-label ="${worker.progress}"
        ></progress>
`)
}

$(document).ready(() => {
    $('#refresh').click(() => {
        $('#error').removeClass("visible")
        updateStatus()
    })
    updateStatus()
})

