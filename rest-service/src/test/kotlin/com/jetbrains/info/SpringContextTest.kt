package com.jetbrains.info

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(
    classes = [RestApplication::class]
)
class SpringContextTest {
    @Test
    fun checkContext() = Unit
}