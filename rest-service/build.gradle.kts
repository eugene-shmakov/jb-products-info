description = "REST Service"

plugins {
    id(Deps.GradlePlugins.springBoot)
}

dependencies {
    implementation(project(Projects.Core.UTILS))
    implementation(project(Projects.Core.DB))
    implementation(project(Projects.Core.MQ))
    implementation(project(Projects.Core.NETWORK))
    implementation(project(Projects.Core.WORKER))
    implementation(project(Projects.Domain.USE_CASES))
    implementation(Deps.Kotlin.stdlib)

    implementation(Deps.Spring.Boot.amqp)
    implementation(Deps.Spring.Boot.web)
    implementation(Deps.Spring.Boot.mongodb)

    implementation(Deps.OpenAPI.ui)
    implementation(Deps.OpenAPI.kotlin)

    //TESTS
    testImplementation(testFixtures(project(Projects.Core.UTILS)))
    testImplementation(Deps.Spring.Boot.test)
    testRuntimeOnly(Deps.Test.JUnit.engine)
}

springBoot {
    mainClass.set("com.jetbrains.info.RestApplicationKt")
}