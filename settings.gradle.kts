import org.gradle.api.initialization.resolve.RepositoriesMode.FAIL_ON_PROJECT_REPOS

@Suppress("UnstableApiUsage")
dependencyResolutionManagement {
    repositoriesMode.set(FAIL_ON_PROJECT_REPOS)
    repositories {
        mavenCentral()
    }
}

rootProject.name = "jb-products-info"
include(
    ":core:utils", ":core:db", ":core:mq", ":core:network", ":core:worker",
    ":domain:model", ":domain:repositories", ":domain:use-cases",
    ":rest-service"
)